<?php

include_once("Model/Model.php");
include_once("View/View.php");
include_once("View/userView.php");

class Controller {

public $Model;

	public function __construct()  
    {  
		//session_start();
        $this->Model = new xmlDom();
    } 

    public function invoke() {


			$skiers = $this->Model->getSkiers(); 

			$clubs = $this->Model->getClubs();

			$seasons = $this->Model->getSeasons();


			$this->Model = new dbModel($db = null, $skiers, $clubs, $seasons);

			$this->Model->addSkiers();
			$this->Model->addClubs(); 
			$this->Model->addSeasons(); 
			
    }
}


?>
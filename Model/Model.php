<?php

include_once('Skiers.php'); 

class xmlDom{

        protected $db = null; 
        protected $doc; 

        public function __construct(){
                
                $this->doc = new DOMDocument(); 
                $this->doc->load('SkierLogs.xml');
            } 


        public function getSkiers() {
            $xpath = new DOMXPath($this->doc);
            $SkiEntriesArray = array();  
            $SkiEntries = $xpath->query("//SkierLogs/Skiers/Skier"); 

            foreach($SkiEntries as $SkiEntry) {
                $userName = $SkiEntry->getAttribute('userName'); 
                $firstName = $SkiEntry->getElementsByTagName('FirstName')->item(0)->textContent; 
                $lastName = $SkiEntry->getElementsByTagName('LastName')->item(0)->textContent; 
                $YearOfBirth = $SkiEntry->getElementsByTagName('YearOfBirth')->item(0)->nodeValue; 
                $SkiEntriesArray[] = new Skier($userName,$firstName,$lastName,$YearOfBirth);        
            }

            return $SkiEntriesArray; 
        }





        public function getClubs() {
            $xpath = new DOMXPath($this->doc);
            $ClubEntriesArray = array();  
            $ClubEntries = $xpath->query("//SkierLogs/Clubs/Club"); 

            foreach($ClubEntries as $ClubEntry) {
                $id = $ClubEntry->getAttribute('id'); 
                $name = $ClubEntry->getElementsByTagName('Name')->item(0)->textContent; 
                $city = $ClubEntry->getElementsByTagName('City')->item(0)->textContent; 
                $county = $ClubEntry->getElementsByTagName('County')->item(0)->textContent; 
                $ClubEntriesArray[] = new Clubs($id, $name, $city, $county);       
            }

                return $ClubEntriesArray; 
            }

            public function getSeasons() {
            $xpath = new DOMXPath($this->doc);
            $SeasonEntriesArray = array();  
            $SeasonEntries = $xpath->query("//SkierLogs/Season"); 

            foreach($SeasonEntries as $SeasonEntry) {
                $fallYear = $SeasonEntry->getAttribute('fallYear'); 
                $clubs = $SeasonEntry->getElementsByTagName('Skiers'); 
                foreach($clubs as $ClubEntry) {
                    $clubID = $ClubEntry->getAttribute('clubId');  
                    $skiers = $ClubEntry->getElementsByTagName('Skier'); 
                    foreach($skiers as $skierEntry){
                        $userName = $skierEntry->getAttribute('userName');
                        $entries = $skierEntry->getElementsByTagName('Entry'); 
                        $totalDistance = 0;
                        foreach($entries as $entry) {
                            $distance = $entry->getElementsByTagName('Distance')->item(0)->nodeValue;
                            $totalDistance += $distance;
                        }   
                        
                        $SeasonEntriesArray[] = new Season($fallYear, $clubID, $userName, $totalDistance);
                    }
                }
            }
                return $SeasonEntriesArray;
        }

}
    

class dbModel {

    protected $db = null; 
    protected $SkiEntriesArray;
    protected $ClubEntriesArray;
    protected $SeasonEntriesArray;


    public function __construct ($db=null, $skiers, $clubs, $seasons) {
       
        $this->SkiEntriesArray = $skiers; 
        $this->ClubEntriesArray = $clubs; 
        $this->SeasonEntriesArray = $seasons; 


           if ($db) 
            {
                $this->db = $db;
            }
            else
            {
                try {                                                                           //If it works: 
                $this->db = new PDO('mysql:host=127.0.0.1;dbname=SkierLogs', 'root', '');        //connection to the database via PDO-object
                //string (what driver we will use, the host and name of database), username(root since running on local computer), password(blank)
                
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);              //Setting a specific error mode
                
                } catch(PDOException $e) {                                                      //If it doesn't work: Catch the error
                    echo $e->getMessage();                                                      //Uses PDO to output the error message (getMessage() function)
                    die();                                                                      //Kill the page
                }

             }
        
        }

        public function addSkiers(){
                    try{
                        $query = $this->db->prepare("INSERT INTO Skier(userName, firstName, lastName, yearOfBirth) VALUES (?, ?, ?, ?)");
                        foreach($this->SkiEntriesArray as $skier) 
                        {
                            $query->execute(array($skier->userName, $skier->firstName, $skier->lastName, $skier->yearOfBirth)); 
                        } 
                    } catch(PDOException $e){
                        echo $e->getMessage(); 

                    }
        } 

        public function addClubs(){
            foreach($this->ClubEntriesArray as $club) {
                try{

                    $query2 = $this->db->prepare("INSERT INTO Location(city, county) VALUES (?,?)");
                    $query1 = $this->db->prepare("INSERT INTO Clubs(id, name, city) VALUES (?,?,?)");

                    $query2->execute(array($club->city, $club->county));     
                    $query1->execute(array($club->id, $club->name, $club->city)); 
                    
                } catch(PDOException $e){
                echo $e->getMessage(); 
                }

            }
        }

            public function addSeasons(){
            try{
                    $query1 = $this->db->prepare("INSERT INTO SkierSeason(fallYear, userName, clubID) VALUES (?,?,?)");
                    $query2 = $this->db->prepare("INSERT INTO SkierSkiedForSeason(userName, fallYear, totalDistance) VALUES (?,?,?)");

                foreach($this->SeasonEntriesArray as $season) 
                { 

                    $query1->execute(array($season->fallYear, $season->userName, $season->clubId)); 
                    $query2->execute(array($season->userName, $season->fallYear, $season->totalDistance)); 
                } 
                } catch(PDOException $e){
                    echo $e->getMessage(); 

                }
        }     


    }


?>